# LeBlogDePapa #


## Code Source / Versions ##

* Dernière version FR (Stable) : [v0.1.0](https://bitbucket.org/4Spartan8/leblogdepapa/src/efa9dc5b26560b4f58749173d16081e34119969b/FR/v0.1.x/?at=master)
* Dernière version FR (en cour de développement) : [v1.0.0](https://bitbucket.org/4Spartan8/leblogdepapa/src/efa9dc5b26560b4f58749173d16081e34119969b/FR/v1.0.x/?at=master)
* Anciennes versions :
--------------------------------
* Dernière version EN (Stable) : [v0.1.0](https://bitbucket.org/4Spartan8/leblogdepapa/src/efa9dc5b26560b4f58749173d16081e34119969b/EN/v0.1.x/?at=master)
* Dernière version EN (en cour de développement) : [v1.0.0](https://bitbucket.org/4Spartan8/leblogdepapa/src/efa9dc5b26560b4f58749173d16081e34119969b/EN/v1.0.x/?at=master)
* Anciennes versions :


### A quoi sert le dépôt ? ###

* Etre tenu au courant des nouvelles versions
* Récupérer le code source de ses dernières
* Contribuer au CMS

### Qui puis-je contacter ? ###

* 4Spartan8 (Développeur & Fondateur)
* Le reste de l'équipe